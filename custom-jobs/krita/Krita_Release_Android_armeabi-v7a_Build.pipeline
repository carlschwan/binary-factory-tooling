// Ask for parameters we will need later on
def buildParameters = input(
	message: 'Which version of Krita is being built?',
	ok: 'Begin Build',
	parameters: [
		string(defaultValue: '', description: '', name: 'Version', trim: true)
	]
)

// Pull the version we've been given out to a separate variable
// We need to do this otherwise Jenkins will throw its toys and claim we are violating a security sandbox which will expose our instance to vulnerabilities
// The Jenkins Security Sandbox is IMO broken and faulty in this regard
def buildVersion = buildParameters ?: ''

// Request a node to be allocated to us
node( "AndroidSDK" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Now we download the release tarball, unpack it and rename the directory to something more convenient to use everywhere else
			sh """
				wget "https://origin.files.kde.org/krita/.release/${buildVersion}/krita-${buildVersion}.tar.gz"

				tar -xf "$WORKSPACE/krita-${buildVersion}.tar.gz"

				mv krita-${buildVersion} krita
			"""
		}

		// Now retrieve the artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'krita-android-deps.tar', projectName: 'Krita_Android_armeabi-v7a_Dependency_Build'

			// Now we unpack them
			sh """
				tar -xf $WORKSPACE/krita-android-deps.tar
			"""
		}

		// Make sure the Android SDK is setup properly
		stage('Setting up SDK') {
			// For this we need to ensure that the Android 24 or later SDK is installed, otherwise the APK generation will fail
			sh """
				sdkmanager "platforms;android-33"
			"""
		}

		// Let's build Krita that we have everything we need
		stage('Building Krita') {
			// The first parameter to the script is where the scripts should work - which is our workspace in this case
			// Otherwise we leave everything in the hands of that script
			sh """
				unset QT_ANDROID
				unset QMAKESPEC

				export ANDROID_ABI=armeabi-v7a
				export ANDROID_API_LEVEL=23
				export CMAKE_ANDROID_NDK=\$ANDROID_NDK_ROOT
				
				krita/packaging/android/androidbuild.sh --src=$WORKSPACE/krita/ --build-type=Release --build-root=$WORKSPACE/build/ -p=krita-bin
			"""
		}

		// Now we can generate the actual APKs!
		stage('Generating Krita APK') {
			// The scripts handle everything here, so just run them
			sh """
				unset QT_ANDROID
				unset QMAKESPEC

				export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/
				export ANDROID_ABI=armeabi-v7a
				export ANDROID_API_LEVEL=23
				export ANDROID_NDK_HOME=\$ANDROID_NDK_ROOT
				export CMAKE_ANDROID_NDK=\$ANDROID_NDK_ROOT

				krita/packaging/android/androidbuild.sh --src=$WORKSPACE/krita/ --build-type=Release --build-root=$WORKSPACE/build/ -p=apk
				
				mv $WORKSPACE/build/krita_build_apk/build/outputs/apk/*/*.apk ./
			"""
		}

		stage('Capturing App Bundle Artifacts') {
			// We only need architecture dependent binaries here, everything else is the same
			sh """
				export ABI=armeabi-v7a
				tar -cf $WORKSPACE/krita-appbundle-artifacts-\$ABI.tar build/krita_build_apk/libs/\$ABI
			"""

			archiveArtifacts artifacts: 'krita-appbundle-artifacts-armeabi-v7a.tar', onlyIfSuccessful: true
		}

		// Finally we capture the APKs for distribution to users
		stage('Capturing APKs') {
			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: '*.apk', onlyIfSuccessful: true
		}
	}
}
}
