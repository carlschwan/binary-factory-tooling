#!/bin/sh
# SPDX-FileCopyrightText: 2021 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: LGPL-2.0-or-later
#
# Install/update Fastlane Supply (https://docs.fastlane.tools/actions/supply/)
# for automatic interaction with the Google Play store, and set up the environment
# accordingly.

#
# Configuration
#
# base directoy in which this script and google-play-sync.py can write
FASTLANE_INSTALL_PREFIX=~
# install location for the fastlane tool
FASTLANE_DIR=$FASTLANE_INSTALL_PREFIX/fastlane
# executable name of Ruby Bundler
BUNDLE_EXECUTABLE=bundle2.7
# path to the folder where this script is located
GEMFILE_TEMPLATE_DIR=~/binary-factory-tooling/android


# setup bundler to no try to install things in the global prefix
export BUNDLE_EXECUTABLE
export BUNDLE_PATH=$FASTLANE_INSTALL_PREFIX/vendor/bundle
export BUNDLE_APP_CONFIG=$FASTLANE_INSTALL_PREFIX/.bundle
$BUNDLE_EXECUTABLE config set --local path $BUNDLE_PATH

# location where the google-play-sync.py script will put its data
export FASTLANE_WORKDIR=$FASTLANE_INSTALL_PREFIX/work

# install/update fastlane
installFastlane()
{
    mkdir -p $FASTLANE_DIR
    prevdir=`pwd`
    cd $FASTLANE_DIR
    cp $GEMFILE_TEMPLATE_DIR/Gemfile.in Gemfile
    $BUNDLE_EXECUTABLE install
    cd $prevdir
}

installFastlane
